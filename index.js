for (let item of document.querySelectorAll('p')) {
    item.style.backgroundColor = '#ff0000';
}

console.log(document.querySelector('#optionsList'));
console.log(document.querySelector('#optionsList').parentElement);

console.log(document.querySelector('#optionsList').childNodes);

document.getElementById('testParagraph').innerText = 'This is paragraph';

for (let item of document.querySelector('.main-header').children) {
    console.log(item);
    item.classList.add('nav-item');
}

for (let item of document.querySelectorAll('.section-title')) {
    item.classList.remove('section-title');
}